require("dotenv").config();
const request = require("request");

const TelegramBot = require("node-telegram-bot-api");
const token = process.env.TOKEN;
const bot = new TelegramBot(token, { polling: true });

// Matches "/movie [movie_name]"
bot.onText(/\/movie (.+)/, (msg, match) => {
  const chatId = msg.chat.id;
  const movie_name = match[1];

  // sample free movie api
  request(
    `https://www.omdbapi.com/?apikey=e2ff4eba&t=${movie_name}`,
    (err, res, body) => {
      if (!err) {
        const info = JSON.parse(body);
        if (info.Response == "False") {
          bot.sendMessage(chatId, "❌" + info.Error);
        }
        // Response == True
        else {
          bot.sendPhoto(chatId, info.Poster, {
            caption: `✅RESULT \nTitle: ${info.Title} \nYear: ${info.Year} \nRated: ${info.Rated} \nReleased: ${info.Released} \nPoster: ${info.Poster}`,
          });
        }
      }
    }
  );
});

bot.on("message", (msg) => {
  const chatId = msg.chat.id;
  const text = msg.text;
  // if (text == "/start") {
  //   bot.sendMessage(chatId, "let's start...");
  // }
});

bot.onText(/(start|\/start)/, (msg, match) => {
  const chatId = msg.chat.id;
  bot.sendSticker(
    chatId,
    "https://tlgrm.eu/_/stickers/a0a/6b0/a0a6b09c-7f38-37e5-9dac-583343142b54/192/1.webp"
  );
  bot.sendMessage(chatId, "let's start... \n/report \n/show \n/exit ");
});

bot.onText(/(exit|\/exit)/, (msg, match) => {
  const chatId = msg.chat.id;
  bot.sendSticker(
    chatId,
    "https://tlgrm.eu/_/stickers/a0a/6b0/a0a6b09c-7f38-37e5-9dac-583343142b54/192/3.webp"
  );
  bot.sendMessage(chatId, "Byeeeeee");
});

bot.onText(/\/report/, (msg, match) => {
  const chatId = msg.chat.id;
  bot.sendMessage(chatId, "Reporting: \n1.----\n2.----\n3.----");
});

bot.onText(/\/show/, (msg, match) => {
  const chatId = msg.chat.id;
  bot.sendMessage(chatId, "Reporting: \na.----\nb.----\nc.----");
});
